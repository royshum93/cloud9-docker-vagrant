# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.box = "bento/ubuntu-18.04"
  config.vm.provision "file", source: ".bash_profile", destination: ".bash_profile"
  config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
  config.vm.synced_folder '.', '/vagrant', mount_options: ["dmode=777,fmode=777"]
  config.vm.provision "docker"
  config.vm.provision "shell", privileged: false, inline: $INSTALL
  config.vm.provision "shell", privileged: false, run: "always", inline: $BOOT
  config.vm.network "private_network", type: "dhcp"
  
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
  end
end


$INSTALL = <<SCRIPT

echo "Install Git"
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update
sudo apt-get install -y git bison dialog build-essential mycli dos2unix
sudo locale-gen "en_US.UTF-8"
sudo systemctl disable systemd-networkd-wait-online.service
sudo systemctl mask systemd-networkd-wait-online.service

sudo dos2unix ~/.bash_profile ~/.gitconfig

echo "Install NVM and Node.js"
mkdir ~/.nvm
export NVM_DIR="$HOME/.nvm"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export PATH="$PATH:$NVM_DIR"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install node
nvm use node
npm i -g npm yarn forever

echo "Install docker-compose"
sudo usermod -aG docker vagrant
sudo curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "Cloning Cloud9 Core"
cd /home/vagrant
git clone https://github.com/c9/core.git Cloud9IDE

echo "Install Cloud9 IDE"
cd Cloud9IDE
scripts/install-sdk.sh

echo "Install Code Server"
curl -s https://api.github.com/repos/cdr/code-server/releases/latest \
  | grep browser_download_url \
  | grep linux \
  | cut -d '"' -f 4 \
  | wget -qi -
tar xvzf *x64.tar.gz --strip 1 --wildcards */code-server
sudo mv ./code-server /usr/bin

SCRIPT


$BOOT = <<SCRIPT
echo "Write swap file"
sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

echo "Start Cloud9 IDE and VS Code server"
cd /home/vagrant/Cloud9IDE
forever start server.js -p 8181 -l 0.0.0.0 -a : -w "/vagrant/workspace"
cd /vagrant/workspace
code-server --disable-telemetry -H -N > /dev/null 2>&1 &
echo "VS Code: http://"$(ifconfig eth1 | grep 'inet ' | awk -F'[ ]+' '{ print $3 }')":8443\n"
echo "Cloud9: http://"$(ifconfig eth1 | grep 'inet ' | awk -F'[ ]+' '{ print $3 }')":8181\n"
SCRIPT
