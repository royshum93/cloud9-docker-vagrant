export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
export TERM="xterm-color"
export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
export NVM_DIR="$HOME/.nvm"
export PATH="$PATH:$NVM_DIR"
export LC_ALL="en_US.UTF-8"

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

function cloud9() {
    forever start /home/vagrant/Cloud9IDE/server.js -w $(pwd) -l 0.0.0.0 -a : $@
}
