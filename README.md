What
====
Cloud9 IDE, VS Code and Docker in Ubuntu 18.04 Vagrant Box. For my own development environment.

How
===
1. Run `vagrant up`.

2. The link of Cloud9 IDE will be shown in the end of `vagrant up` output.

Tips
====
- In VM terminal, type `cloud9 -p $PORT` to create new instance of Cloud9 for the current folder on port `$PORT`.
